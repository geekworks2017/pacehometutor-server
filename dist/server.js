'use strict';

var hostname = '127.0.0.1';
var port = 3000;

var server = require('./controllers/controller.js');

server.listen(port, hostname, function () {
    console.log('Server running at http://' + hostname + ':' + port + '/');
});